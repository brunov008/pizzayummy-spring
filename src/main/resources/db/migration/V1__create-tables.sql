CREATE TABLE IF NOT EXISTS `pizza`.`PizzaOrder` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `address` VARCHAR(50) NOT NULL,
  `total` DECIMAL(10,2) NOT NULL,
  `deliveryDate` DATETIME NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pizza`.`Pizza` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `price` DECIMAL(10,2) NULL,
  `orders` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE,
  INDEX `FK_pizza_order_idx` (`orders` ASC) VISIBLE,
  CONSTRAINT `FK_pizza_order`
    FOREIGN KEY (`orders`)
    REFERENCES `pizza`.`PizzaOrder` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pizza`.`Topping` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `pizza` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_topping_pizza_idx` (`pizza` ASC) VISIBLE,
  CONSTRAINT `FK_topping_pizza`
    FOREIGN KEY (`pizza`)
    REFERENCES `pizza`.`Pizza` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `pizza`.`Base` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `pizza` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_base_pizza_idx` (`pizza` ASC) VISIBLE,
  CONSTRAINT `FK_base_pizza`
    FOREIGN KEY (`pizza`)
    REFERENCES `pizza`.`Pizza` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
