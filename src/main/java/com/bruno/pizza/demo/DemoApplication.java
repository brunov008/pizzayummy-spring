package com.bruno.pizza.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.bruno.pizza.demo.domain.model.Pizza;
import com.bruno.pizza.demo.repository.PizzaRepository;

import java.util.Random;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class DemoApplication {
	
	private static final Logger log = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		
		Flyway flyway = Flyway
				.configure()
				.dataSource("jdbc:mysql://localhost:3306/pizza", "root", "admin123")
				.load();
		
		flyway.migrate();
	}

	@Bean
	public CommandLineRunner demo(PizzaRepository repository) {
		return (args) -> {		
			try {
				Float randomNumber = new Random().nextFloat();
				
				Pizza p1 = new Pizza("Pizza "+ Math.round(randomNumber)*100 , randomNumber*150); //Only test
				
				repository.save(p1);
			}catch(Exception ex) {
				System.out.println("Item já cadastrado");
			}
			
			//repository.findById(1L).ifPresent(arg -> log.info(arg.getName()));
		};
	}
}
