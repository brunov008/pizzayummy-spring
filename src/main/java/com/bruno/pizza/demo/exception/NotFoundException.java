package com.bruno.pizza.demo.exception;

public class NotFoundException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public NotFoundException() {
		super();
	}
	
	public NotFoundException(String message) {
		super(message);
	}
	
	public NotFoundException(Throwable t) {
		super(t);
	}
	
}
