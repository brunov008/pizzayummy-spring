package com.bruno.pizza.demo.exception;

import java.sql.SQLIntegrityConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bruno.pizza.demo.domain.model.BasesEnum;

@ControllerAdvice
public class ExceptionController {
	
	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public <T> ResponseEntity<T> handleCustomException(NotFoundException ex) {
		return new ResponseEntity<T> ((T)ex.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	//Validacao Entidade hibernate @NotNull @NotBlank etc...
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public <T> ResponseEntity<T> handleCustomException(MethodArgumentNotValidException ex) {
		return new ResponseEntity<T> ((T)"Erro de validacao",HttpStatus.BAD_REQUEST);
	}
	
	//Validacao chave unica
	@ExceptionHandler(SQLIntegrityConstraintViolationException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public <T> ResponseEntity<T> handleCustomException(SQLIntegrityConstraintViolationException ex) {
		return new ResponseEntity<T> ((T)"Item já cadastrado",HttpStatus.CONFLICT);
	}
	
	//Enum Hibernate
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public <T> ResponseEntity<T> handleCustomException(HttpMessageNotReadableException ex) {
		String message = "As massas devem ser: ";
		String massas = "";
	
		for(BasesEnum value: BasesEnum.values()) {
			massas += value + " ";
		}
		
		String ok = message + massas;
		
		return new ResponseEntity<T> ((T)ok,HttpStatus.BAD_REQUEST);
	}
	
}
