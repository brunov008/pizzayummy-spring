package com.bruno.pizza.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bruno.pizza.demo.domain.model.Pizza;
import com.bruno.pizza.demo.domain.model.PizzaOrder;

@Repository
public interface PizzaOrderRepository extends JpaRepository<PizzaOrder, Long>{
	
}
