package com.bruno.pizza.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bruno.pizza.demo.domain.model.Topping;

@Repository
public interface ToppingRepository extends JpaRepository<Topping, Long>{

}
