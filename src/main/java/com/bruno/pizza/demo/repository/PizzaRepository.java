package com.bruno.pizza.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bruno.pizza.demo.domain.model.Pizza;

@Repository
public interface PizzaRepository extends JpaRepository<Pizza, Long>{
	public List<Pizza> getByIds(List<Integer> ids);
}
