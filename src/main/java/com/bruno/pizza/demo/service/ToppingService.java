package com.bruno.pizza.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bruno.pizza.demo.domain.model.Topping;
import com.bruno.pizza.demo.repository.ToppingRepository;

@Service
public class ToppingService {
	
	@Autowired
	private ToppingRepository toppingRepository;
	
	@Transactional
	public Topping save(Topping topping) {
		return toppingRepository.save(topping);
	}
}
