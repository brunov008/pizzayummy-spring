package com.bruno.pizza.demo.service;

import java.util.ArrayList;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bruno.pizza.demo.domain.model.Pizza;
import com.bruno.pizza.demo.domain.model.PizzaOrder;
import com.bruno.pizza.demo.repository.PizzaOrderRepository;
import com.bruno.pizza.demo.repository.PizzaRepository;

@Service
public class OrderService {
	
	@Autowired
	private PizzaOrderRepository orderRepository;
	
	@Autowired
	private PizzaRepository pizzaRepository;
	
	@Transactional
	public ArrayList<Pizza> savePizzasInOrder(ArrayList<Integer> ids){
		
		return (ArrayList<Pizza>)pizzaRepository.getByIds(ids);	
	}
	
	@Transactional
	public PizzaOrder save(PizzaOrder pizza) {
		return orderRepository.save(pizza);
	}
	
	@Transactional
	public void delete(Long id) {
		orderRepository.deleteById(id);
	}
}
