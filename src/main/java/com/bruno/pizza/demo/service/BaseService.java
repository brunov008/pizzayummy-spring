package com.bruno.pizza.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bruno.pizza.demo.domain.model.Base;
import com.bruno.pizza.demo.repository.BaseRepository;

@Service
public class BaseService {
	
	@Autowired
	private BaseRepository baseRepository;
	
	@Transactional
	public Base save(Base base) {
		return baseRepository.save(base);
	}
}
