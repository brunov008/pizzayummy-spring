package com.bruno.pizza.demo.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bruno.pizza.demo.domain.model.Pizza;
import com.bruno.pizza.demo.repository.PizzaRepository;

@Service
public class PizzaService {
	
	@Autowired
	private PizzaRepository pizzaRepository;
	
	@Transactional
	public Pizza save(Pizza pizza) {
		return pizzaRepository.save(pizza);
	}
	
	@Transactional
	public void delete(Long id) {
		pizzaRepository.deleteById(id);
	}

}
