package com.bruno.pizza.demo.controller;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bruno.pizza.demo.api.model.BaseResponse;
import com.bruno.pizza.demo.domain.model.Base;
import com.bruno.pizza.demo.repository.BaseRepository;
import com.bruno.pizza.demo.service.BaseService;

@RestController
@RequestMapping("/bases")
public class BaseController {
	
	@Autowired
	private BaseRepository baseRepository;
	
	@Autowired
	private BaseService baseService;
	
	@GetMapping
	public List<Base> listar(){
		List<Base> list = baseRepository
				.findAll()
				.parallelStream()
				.collect(Collectors.toList());
		
		Collections.reverse(list);
		
		return list;
	}
	
	// Teste com Enum
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public BaseResponse save(@Valid @RequestBody Base body){
		Base response = baseService.save(body);
		
		return new BaseResponse("Massa salva com sucesso");
	}
}
