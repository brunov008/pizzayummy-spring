package com.bruno.pizza.demo.controller;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bruno.pizza.demo.api.model.BaseResponse;
import com.bruno.pizza.demo.domain.model.Topping;
import com.bruno.pizza.demo.repository.ToppingRepository;
import com.bruno.pizza.demo.service.ToppingService;

@RestController
@RequestMapping("/toppings")
public class ToppingController {
	@Autowired
	private ToppingRepository toppingRepository;
	
	@Autowired
	private ToppingService toppingService;
	
	@GetMapping
	public List<Topping> listar(){
		List<Topping> list = toppingRepository
				.findAll()
				.parallelStream()
				.collect(Collectors.toList());
		
		Collections.reverse(list);
		
		return list;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public BaseResponse save(@Valid @RequestBody Topping body){
		Topping response = toppingService.save(body);
		
		return new BaseResponse("Cobertura salva com sucesso");
	}
}
