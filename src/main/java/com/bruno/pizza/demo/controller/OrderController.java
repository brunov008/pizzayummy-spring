package com.bruno.pizza.demo.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bruno.pizza.demo.api.model.BaseResponse;
import com.bruno.pizza.demo.domain.model.Pizza;
import com.bruno.pizza.demo.domain.model.PizzaOrder;
import com.bruno.pizza.demo.exception.NotFoundException;
import com.bruno.pizza.demo.repository.PizzaOrderRepository;
import com.bruno.pizza.demo.service.OrderService;

@RestController
@RequestMapping("/orders")
public class OrderController {
	
	@Autowired
	private PizzaOrderRepository orderRepository;
	
	@Autowired
	private OrderService orderService;
	
	@PutMapping("/{idOrder}")
	public BaseResponse addPizzaInOrder(@PathVariable Long idOrder, @RequestBody ArrayList<Integer> body) throws NotFoundException {
		orderRepository
			.findById(idOrder)
			.ifPresentOrElse(args -> orderService.savePizzasInOrder(body), () -> new NotFoundException("Ocorreu um erro"));
		
		return new BaseResponse("Operação realizada com sucesso");
	}
	
	@GetMapping
	public List<PizzaOrder> listar(){	
		List<PizzaOrder> list = orderRepository
				.findAll()
				.parallelStream()
				.map(args -> {
					//Do some Stuff
					//args.setPrice((float)0);
					return args;
				})
				.collect(Collectors.toList());
		
		Collections.reverse(list);
		
		return list;
	}
	
	//FIXME Review
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public BaseResponse save(@Valid @RequestBody PizzaOrder body){
		PizzaOrder response = orderService.save(body);
				
		return new BaseResponse("Pedido salvo com sucesso");
	}
	
	@GetMapping("/{idOrder}")
	public PizzaOrder getOne(@PathVariable Long idOrder) throws NotFoundException {
		return orderRepository
				.findById(idOrder)
				.orElseThrow(() -> new NotFoundException("Pedido não encontrado."));
		
	}
	
	//FIXME Review
	@PutMapping("/{idOrder}")
	public BaseResponse updateOne(@PathVariable Long idOrder, @Valid @RequestBody PizzaOrder body) throws NotFoundException{
		
		return orderRepository.findById(idOrder)
			.map((PizzaOrder args) -> {
				body.setId(idOrder);
				orderRepository.save(body);
				return new BaseResponse("Dados atualizados com sucesso!");
			})
			.orElseThrow(() -> new NotFoundException("Ocorreu um erro ao atualizar o pedido"));
	}
	
	@DeleteMapping("/{idOrder}")
	public BaseResponse delete(@PathVariable Long idOrder) throws NotFoundException {
		return orderRepository.findById(idOrder)
				.map((PizzaOrder args) -> {
					orderService.delete(idOrder);
					return new BaseResponse("Pizza excluida com sucesso!");
				})
				.orElseThrow(() -> new NotFoundException("Pedido não encontrado."));
	}

}
