package com.bruno.pizza.demo.controller;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bruno.pizza.demo.api.model.BaseResponse;
import com.bruno.pizza.demo.domain.model.Pizza;
import com.bruno.pizza.demo.exception.NotFoundException;
import com.bruno.pizza.demo.repository.PizzaRepository;
import com.bruno.pizza.demo.service.PizzaService;

@RestController
@RequestMapping("/pizzas")
public class PizzaController {
	
	@Autowired
	private PizzaRepository pizzaRepository;
	
	@Autowired
	private PizzaService pizzaService;
	
	@GetMapping
	public List<Pizza> listar(){	
		List<Pizza> list = pizzaRepository
				.findAll()
				.parallelStream()
				.peek(args -> System.out.println(args.getName()))
				.map(args -> {
					//Do some Stuff
					//args.setPrice((float)0);
					return args;
				})
				.collect(Collectors.toList());
		
		Collections.reverse(list);
		
		return list;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public BaseResponse save(@Valid @RequestBody Pizza body){
		Pizza response = pizzaService.save(body);
				
		return new BaseResponse("Pizza salva com sucesso");
	}
	
	@GetMapping("/{idPizza}")
	public Pizza getOne(@PathVariable Long idPizza) throws NotFoundException {
		return pizzaRepository
				.findById(idPizza)
				.orElseThrow(() -> new NotFoundException("Ocorreu um erro"));
		
	}
	
	@PutMapping("/{idPizza}")
	public BaseResponse updateOne(@PathVariable Long idPizza, @Valid @RequestBody Pizza body) throws NotFoundException{
		
		return pizzaRepository.findById(idPizza)
			.map((Pizza args) -> {
				body.setId(idPizza);
				pizzaRepository.save(body);
				return new BaseResponse("Dados atualizados com sucesso!");
			})
			.orElseThrow(() -> new NotFoundException("Ocorreu um erro ao atualizar"));
	}
	
	@DeleteMapping("/{idPizza}")
	public BaseResponse delete(@PathVariable Long idPizza) throws NotFoundException {
		return pizzaRepository.findById(idPizza)
				.map((Pizza args) -> {
					pizzaService.delete(idPizza);
					return new BaseResponse("Pizza excluida com sucesso!");
				})
				.orElseThrow(() -> new NotFoundException("Ocorreu um erro"));
	}
}
